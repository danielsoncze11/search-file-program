# This program helps you to search any file in any PC location
# The output of the program is list of paths and file names which matches the search criteria
# Program also provides information for each folder which can not be searched

import os
import re

def main():
    welcome()
    folder = get_folder_from_user()

    # To test if the user provided valid folder (if anything was provided, if the folder exist)
    # the potential none/false comes from the get_folder_from_user
    if not folder:
        print('You did not provided a valid folder')
        return

    searched_file = get_the_searched_file()

    # To test if the user provided valid text (if anything was provided)
    if not searched_file:
        print('You did not provide a file name')
        return

    matches = search_folders(folder, searched_file)

    for m in matches:
        print(m)

def welcome():
    print('--------------------------------------')
    print('------------FILE SEARCHER-------------')
    print('--------------------------------------\n')

def get_folder_from_user():

    folder = input('Please provide the directory where to search:')

    # if the user provided nothing or white space
    if not folder or not folder.strip():
        return None

    # if the folder does not exists
    if not os.path.isdir(folder):
        return None

    # returns absoluth path of the folder what user provided
    return os.path.abspath(folder)

def get_the_searched_file():

    file = input("Please provide the full file name or use '*' wildcard to search for any character:")

    if not file or not file.strip():
        return None

    return file

def search_folders(folder, searched_file):

    # list of all items (folders, files,..) what is in the respective directory/folder
    items = os.listdir(folder)

    for item in items:

        # to get the full path of the item
        # join the absoluth path of the folder and the item
        full_item = os.path.join(folder, item)

        # check if the item in directory is directory
        try:
            if os.path.isdir(full_item):
                yield from search_folders(full_item, searched_file)

            else:
                yield from look_for_file(folder, item, searched_file)

        except PermissionError as e:
            print('{}'.format(e))
            pass

def look_for_file(folder, item, searched_file):

    # change the search file into re form in case the wildcard * is used
    searched_file_for_re = searched_file.replace('*', '.+')
    # print(searched_file_for_re)
    check_the_re_file = re.findall(searched_file_for_re, item)

    # check if the searched object matches the searched file
    if len(check_the_re_file) > 0:
        full_item = os.path.join(folder, item)
        yield full_item


if __name__ == '__main__':
    main()

