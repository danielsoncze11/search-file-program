# Example of the scraping spider for Scrapy framework to scrape
# the masschallenge.org web to get the information about startups

# coding: utf8

import scrapy

class ChallengeSpider(scrapy.Spider):
    name = 'ChallengeSpider'
    start_urls = ['https://masschallenge.org/startups']

    def parse(self, response):
        startup_pages = response.xpath("//div[@class='card startup']/a/@href")

        for startup_page in startup_pages:
            startup_page_url = startup_page.get()
            yield response.follow(startup_page_url, self.parse_startup_page)

    def parse_startup_page(self, response):
        startup_links = response.xpath("//div[@class='startup-links']/a/@href")
        startup_specialization_first = response.xpath("//div[@class='field-items']/child::node()/a/text()").extract_first()
        startup_specialization_second = response.xpath("//div[@class='field-item']/p/text()").extract_first()
        startup_specialization_third = response.xpath("//div[@class='field-item']/text()").getall()
        startup_specialization_third_formatted = str("|".join(startup_specialization_third))

        yield {'name': response.xpath("//h5[@class='card-title']/text()").extract_first(),
               'description': response.xpath("//div[@class='startup-fullelevatorpitch']/text()").extract_first(),
               'oneliner': response.xpath("//div[@class='startup-tweetablepitch']/text()").extract_first(),
               'url': startup_links[-1].get(),
               'facebook': response.xpath("//i[@class='fa fa-facebook']/parent::node()/@href").get(),
               'linkedin': response.xpath("//i[@class='fa fa-linkedin']/parent::node()/@href").get(),
               'twitter': response.xpath("//i[@class='fa fa-twitter']/parent::node()/@href").get(),
               'email': response.xpath("//i[@class='fa fa-envelope-o']/parent::node()/@href").re(r'mailto:\s*(.*)'),
               'logo': response.xpath("//div[@class='startup-detail-logo']/child::node()/@src").get(),
               'specialization': startup_specialization_first + '|' + startup_specialization_second + '|' + startup_specialization_third_formatted,
               'detail': response.xpath("//meta[@property='og:url']/@content").get()
               }