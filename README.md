This repository contains my programs:

- **scraping_spider_for_Scrapy** - Scrapy spider for scraping information about Start-ups company information
- **search_file_program** - file searcher program
- **snow_searcher_app** - web scraping program to get the information about places with most fresh snow