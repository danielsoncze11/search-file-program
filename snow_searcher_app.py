# This app provides the latest information about the powder situation in Alps
# The powder information can be provided for 5 countries
# The powder information is scraped from  bergfex.at

import requests
import os
import re
from bs4 import BeautifulSoup

print('-------------------------------------------------------')
print('--------Top 10 Ski resorts with most powder App--------')
print('-------------------------------------------------------\n')
print('You can get the fresh pow pow report from Austria, Italy, France, Switzerland or Czech.')
print()
print('Type the first letter of the country you would like to see')

folder = 'https://www.bergfex.at/'
forecast = 'schneewerte/'

# get the country from the user
user_input = input(str('[A]ustria, [I]taly, [F]rance, [S]witzerland, [C]zech:'))
print()
user_input = user_input.lower()

# check if the use provided correct input
check_the_input = r"[aifcs]"
if not user_input or not re.match(check_the_input, user_input):
    print('You did not provide any/correct country. Provide a correct country symbol')
    exit()

# get the full url
list_of_countries = {
    'a': 'oesterreich/',
    'i': 'italien/',
    'f': 'frankreich/',
    'c': 'tschechien/',
    's': 'schweiz/'
}

snow_forecast_country = list_of_countries.get(user_input)
url = os.path.join(folder,  snow_forecast_country, forecast)

source = requests.get(url).text
soup = BeautifulSoup(source, 'lxml')

a = soup.body
b = a.find("div", id = "page-container")
c = b.find("main", class_ = "page-left-margin")
d = c.find("div", class_ = "content")
e = d.find("div", class_ = "section-full")
f = e.find("div", class_ = "section touch-scroll-x")
g = f.find("table")
h = g.find("tbody")
i = h.find_all("tr", class_= "tr0")
k = h.find_all("tr", class_= "tr1")

dict_of_resorts_with_new_snow = {}

def create_dict_of_resorts_with_new_snow(x):
    for row in x:
    # each row has the first td tag which is the resort name
        resort_name = row.td.text.strip()

    #each row has 4 td tag information about snow and time of the record
        j = row.find_all("td", class_="right nowrap")

    # if there is a value it will be stored as value, if not it will be 0
        if len(j[0]['data-value']) > 0:
            tal_snow_value = int(j[0]['data-value'])
        else:
            tal_snow_value = int(0)

        if len(j[1]['data-value']) > 0:
            berg_snow_value = int(j[1]['data-value'])
        else:
            berg_snow_value = int(0)

        if len(j[2]['data-value']) > 0:
            new_snow_value = int(j[2]['data-value'])
        else:
            new_snow_value = int(0)

        when_snow_value = j[3].text.strip()

    # add to dict of all resorest...but only resort with one value
        dict_of_resorts_with_new_snow[resort_name] = [tal_snow_value, berg_snow_value, new_snow_value, when_snow_value]

create_dict_of_resorts_with_new_snow(i)
create_dict_of_resorts_with_new_snow(k)

# create a function to determine by what item we will sort dict in the next print
def by_value(item):
    return item[1][2]

# print resorts with the snow in the sorted order with the most snow...sorted by value
check_count = 1
for key, value in sorted(dict_of_resorts_with_new_snow.items(), key=by_value, reverse=True):
    if check_count < 11:
        print('{} - cm of fresh snow in {}. Data as of: {}'.format(value[2], key, value[3]))
    check_count += 1